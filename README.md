we used the following resources in order to develop the methods:

Survey sur le DNN Watermarking -> https://www.frontiersin.org/articles/10.3389/fdata.2021.729663/pdf
White-Box, Uchida et al. -> https://arxiv.org/pdf/1701.04082
Black-Box, Zhang et al. -> https://www.researchgate.net/profile/Zhongshu-Gu/publication/325480419_Protecting_Intellectual_Property_of_Deep_Neural_Networks_with_Watermarking/links/5c1cfcd4a6fdccfc705f2cd4/Protecting-Intellectual-Property-of-Deep-Neural-Networks-with-Watermarking.pdf



-----------------------------> IMPLEMENTATION
              .               .                   .             .         .
The code aims to be as clear and readable as possible with every part explained. we use ipynb files to separate codes into cells with explanations linked to each cell for an ease of access.
Throughout the code you will find #TOMOD these are lines of code that you are asked to modify in order for the code to function properly or to set the preference of the user. They are generally save paths / data directories/ coefficients...
-------> Data tainting
    Tainting methods are present in the tainting_methods Modify the #TOMODS before running the cells. the code will create folders with tainted data that you can use 

-------> Proof of concept : 
                            Uchida method
                    

----------------------------->Credits
This work is the result of the combined efforts of : Mohamed Salim Arifa
                                                    Noé Getuirrez
                                                    Houda Ghallab

This work was managed by our tutor mr. Mohamed Lansari

Some of the art used in the project was generously provided by : -Mohamed Attia (@pope.art)